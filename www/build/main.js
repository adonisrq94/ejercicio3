webpackJsonp([0],{

/***/ 107:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 107;

/***/ }),

/***/ 148:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 148;

/***/ }),

/***/ 192:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"/home/adonisrq94/Escritorio/ejercicio-3/src/pages/home/home.html"*/'<app-header></app-header>\n<app-formulario1></app-formulario1>\n'/*ion-inline-end:"/home/adonisrq94/Escritorio/ejercicio-3/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FactoriaFormulario; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__classes_FormularioUno__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__classes_FormularioDos__ = __webpack_require__(267);


var FactoriaFormulario = (function () {
    function FactoriaFormulario(navC) {
        this.navC = navC;
    }
    FactoriaFormulario.prototype.getFormularioUno = function () {
        return new __WEBPACK_IMPORTED_MODULE_0__classes_FormularioUno__["a" /* FormularioUno */](this.navC);
    };
    FactoriaFormulario.prototype.getFormularioDos = function (complete) {
        return new __WEBPACK_IMPORTED_MODULE_1__classes_FormularioDos__["a" /* FormularioDos */](complete);
    };
    return FactoriaFormulario;
}());

//# sourceMappingURL=formulario.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Formulario; });
var Formulario = (function () {
    function Formulario() {
    }
    Formulario.prototype.validacionesExtras = function (dato) {
        return true;
    };
    Formulario.prototype.presentConfirm = function (form, alertCtrl) {
    };
    return Formulario;
}());

//# sourceMappingURL=formulario.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Formulario2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_registrar_service__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__factories_formulario__ = __webpack_require__(193);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Formulario2Page = (function () {
    function Formulario2Page(_registrar, navC, NavParams, alertCtrl, formBuilder) {
        this.navC = navC;
        this.NavParams = NavParams;
        this.alertCtrl = alertCtrl;
        this.formBuilder = formBuilder;
        this.factoriaFormulario = new __WEBPACK_IMPORTED_MODULE_4__factories_formulario__["a" /* FactoriaFormulario */](this.navC);
        this.formulario = this.factoriaFormulario.getFormularioDos(this.NavParams.data.form.opcion);
        this.formulario2 = this.formulario.GetFormBuilder();
        this.ciudades = _registrar.getApiGoogleCiudad();
        this.paises = _registrar.getApiGoogleaPais();
    }
    Formulario2Page.prototype.onChange = function (deviceValue) {
        console.log(deviceValue);
    };
    Formulario2Page.prototype.showAlert = function () {
        this.formulario.presentConfirm(this.formulario, this.alertCtrl);
    };
    return Formulario2Page;
}());
Formulario2Page = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-formulario2',template:/*ion-inline-start:"/home/adonisrq94/Escritorio/ejercicio-3/src/pages/formularios-pages/formulario2/formulario2.html"*/'<app-header></app-header>\n<ion-content class="card-formulario">\n<ion-card>\n <form [formGroup]="formulario2" (ngSubmit)="showAlert()" >\n<ion-grid>\n<ion-row>\n\n  <ion-col col-11>\n    <img src="assets/img/logo.jpg" class="logo img-fluid" alt="...">\n  </ion-col>\n  <ion-col class="titulo centrado" col-12><h1>2/2</h1></ion-col>\n  <ion-col class="titulo2 centrado" col-12>Direcciones y Contactos</ion-col>\n\n\n     <ion-col col-12  >\n       <ion-item>\n       <ion-label class=" centrado" stacked>Pais</ion-label>\n       <ion-select  placeholder="Venezuela"class="inputs centrado" formControlName="pais"  (change)="onChange($event.target.value)">\n       <ion-option  [value]="pais.name"  selected="true" *ngFor="let pais of paises">\n         {{pais.name}}</ion-option>\n     </ion-select>\n       </ion-item>\n     </ion-col >\n\n\n\n\n     <ion-item  class="centrado">\n     <ion-label class=" centrado" stacked>Direccion</ion-label>\n     <ion-input class="inputs" type="text" formControlName="direccion"\n      placeholder="Urb Lorem calle 128 Apt 12 piso 2"></ion-input>\n     </ion-item>\n\n\n     <ion-col col-6  >\n       <ion-item>\n       <ion-label class=" centrado" stacked>Ciudad</ion-label>\n       <ion-select  placeholder="Caracas" class="inputs  centrado" formControlName="ciudad">\n       <ion-option value="caracas" selected="true">Caracas</ion-option>\n       <ion-option value="tachira">Tachira</ion-option>\n     </ion-select>\n       </ion-item>\n     </ion-col >\n\n\n          <ion-col col-6>\n            <ion-item  class="centrado">\n            <ion-label class=" centrado" stacked>Cod. postal</ion-label>\n            <ion-input class="inputs" type="text" formControlName="cod"></ion-input>\n            </ion-item>\n          </ion-col >\n\n\n\n\n\n\n\n</ion-row>\n<ion-row>\n  <ion-col col-12 class="boton-siguiente">\n  <button type="submit" class="button-block boton button"ion-button round [disabled]="!formulario2.valid">Continuar</button>\n  </ion-col>\n</ion-row>\n\n\n</ion-grid>\n</form>\n'/*ion-inline-end:"/home/adonisrq94/Escritorio/ejercicio-3/src/pages/formularios-pages/formulario2/formulario2.html"*/
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_registrar_service__["a" /* RegistrarService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_registrar_service__["a" /* RegistrarService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavParams */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]) === "function" && _e || Object])
], Formulario2Page);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=formulario2.js.map

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(215);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_formularios_pages_formulario1_formulario1__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_formularios_pages_formulario2_formulario2__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_header_header__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_registrar_service__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_formularios_pages_formulario1_formulario1__["a" /* Formulario1Page */],
            __WEBPACK_IMPORTED_MODULE_8__pages_formularios_pages_formulario2_formulario2__["a" /* Formulario2Page */],
            __WEBPACK_IMPORTED_MODULE_9__pages_header_header__["a" /* HeaderComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */])
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_formularios_pages_formulario1_formulario1__["a" /* Formulario1Page */],
            __WEBPACK_IMPORTED_MODULE_8__pages_formularios_pages_formulario2_formulario2__["a" /* Formulario2Page */],
            __WEBPACK_IMPORTED_MODULE_9__pages_header_header__["a" /* HeaderComponent */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_10__services_registrar_service__["a" /* RegistrarService */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(192);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/home/adonisrq94/Escritorio/ejercicio-3/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/adonisrq94/Escritorio/ejercicio-3/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 265:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Formulario1Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__factories_formulario__ = __webpack_require__(193);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Formulario1Page = (function () {
    function Formulario1Page(navC, alertCtrl) {
        this.navC = navC;
        this.alertCtrl = alertCtrl;
        this.factoriaFormulario = new __WEBPACK_IMPORTED_MODULE_2__factories_formulario__["a" /* FactoriaFormulario */](this.navC);
        this.formulario = this.factoriaFormulario.getFormularioUno();
        this.formulario1 = this.formulario.GetFormBuilder();
    }
    Formulario1Page.prototype.irFormulario2 = function (form) {
        if (form === void 0) { form = this.formulario1.value; }
        if (this.formulario.validacionesExtras(form.fecha)) {
            this.formulario.presentConfirm(form, this.alertCtrl);
        }
        else {
            this.formulario1.reset();
        }
    };
    return Formulario1Page;
}());
Formulario1Page = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-formulario1',template:/*ion-inline-start:"/home/adonisrq94/Escritorio/ejercicio-3/src/pages/formularios-pages/formulario1/formulario1.html"*/'\n\n<ion-content class="card-formulario">\n<ion-card>\n <form  [formGroup]="formulario1" (ngSubmit)="irFormulario2()">\n<ion-grid>\n<ion-row>\n\n  <ion-col col-11>\n    <img src="assets/img/logo.jpg" class="logo img-fluid" alt="...">\n  </ion-col>\n  <ion-col class="titulo centrado" col-12><h1>1/2</h1></ion-col>\n  <ion-col class="titulo2 centrado" col-12>Datos Personales</ion-col>\n  <ion-col col-6>\n  <ion-item >\n  <ion-label class=" centrado" stacked>Nombre</ion-label>\n  <ion-input class="inputs" type="text" placeholder="Maria" formControlName="nombre"></ion-input>\n  </ion-item>\n  </ion-col>\n  <ion-col col-6>\n  <ion-item>\n  <ion-label class=" centrado" stacked>Apellido</ion-label>\n  <ion-input class="inputs" type="text" placeholder="Lopez" formControlName="apellido"></ion-input>\n  </ion-item>\n  </ion-col>\n  <ion-item>\n  <ion-label class=" centrado" stacked>Correo</ion-label>\n  <ion-input class="inputs" type="text" placeholder="Lopez@gmail.com" formControlName="correo"></ion-input>\n  </ion-item>\n  <ion-item>\n  <ion-label class=" centrado" stacked>Fecha nacimiento</ion-label>\n  <ion-datetime  class="inputs " formControlName="fecha" on-touch = "validaciones()"\n  displayFormat="MM  DD  YYYY" pickerFormat="MM  DD YYYY" ></ion-datetime>\n  </ion-item>\n  <ion-col col-6>\n  <ion-item>\n  <ion-label class=" centrado" stacked>Genero</ion-label>\n  <ion-select   placeholder="Masculino"\n  class="inputs  centrado" formControlName="genero">\n  <ion-option  value="Masculino" selected="true" >Masculino</ion-option>\n  <ion-option value="Femenino">Femenino</ion-option>\n  </ion-select>\n  </ion-item>\n  </ion-col>\n  <ion-col col-6>\n    <ion-item>\n    <ion-label class=" centrado" stacked>Identificacion</ion-label>\n    <ion-input class="inputs" type="text" placeholder="15685" formControlName="identificacion"></ion-input>\n    </ion-item>\n  </ion-col>\n</ion-row>\n<ion-row>\n  <ion-col col-12 class="boton-siguiente">\n  <button type="submit" block class="button-block boton button"\n  ion-button round  [disabled]="formulario1.valid" >Siguiente</button>\n  </ion-col>\n</ion-row>\n\n\n</ion-grid>\n</form>\n\n\n\n\n\n\n</ion-card>\n</ion-content>\n'/*ion-inline-end:"/home/adonisrq94/Escritorio/ejercicio-3/src/pages/formularios-pages/formulario1/formulario1.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], Formulario1Page);

//# sourceMappingURL=formulario1.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormularioUno; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__formulario__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_formularios_pages_formulario2_formulario2__ = __webpack_require__(195);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var FormularioUno = (function (_super) {
    __extends(FormularioUno, _super);
    function FormularioUno(navC) {
        var _this = _super.call(this) || this;
        _this.navC = navC;
        _this.formBuilder = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */]();
        return _this;
    }
    FormularioUno.prototype.GetFormBuilder = function () {
        this.form = this.formBuilder.group({
            nombre: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern("^[a-zA-Z_áéíóúñ\s]*$")]],
            apellido: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern("^[a-zA-Z_áéíóúñ\s]*$")]],
            genero: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            fecha: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            identificacion: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].minLength(1), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].maxLength(5), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern("[0-9]*")]],
            correo: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern('^[a-z0-9A-Z_]+(\.[_a-z0-9A-Z]+)*@[a-z0-9-A-Z]+(\.[a-z0-9-A-Z]+)*(\.[a-zA-Z]{2,15})$')]],
        });
        return this.form;
    };
    FormularioUno.prototype.presentConfirm = function (form, alertCtrl) {
        var _this = this;
        var alert = alertCtrl.create({
            title: 'Confirmar',
            message: 'Deseas ingresar la direccion detallada?',
            buttons: [
                {
                    text: 'si',
                    role: 'cancel',
                    handler: function () {
                        form.opcion = true;
                        _this.navC.push(__WEBPACK_IMPORTED_MODULE_2__pages_formularios_pages_formulario2_formulario2__["a" /* Formulario2Page */], { form: form });
                    }
                },
                {
                    text: 'no',
                    handler: function () {
                        form.opcion = false;
                        _this.navC.push(__WEBPACK_IMPORTED_MODULE_2__pages_formularios_pages_formulario2_formulario2__["a" /* Formulario2Page */], { form: form });
                    }
                }
            ]
        });
        alert.present();
    };
    FormularioUno.prototype.validarEdad = function (date) {
        //validamos que sea mayor de edad
        date = date.replace('-', '/');
        date = date.replace('-', '/');
        var birthday = new Date(date);
        var year = birthday.getFullYear();
        var month = birthday.getMonth();
        var day = birthday.getDate();
        var dateToday = new Date();
        var yearToday = dateToday.getFullYear();
        var monthToday = dateToday.getMonth();
        var dayToday = dateToday.getDate();
        var age = (yearToday + 1900) - year;
        if (monthToday < (age - 1)) {
            age--;
        }
        if (((month - 1) == monthToday) && (dayToday < day)) {
            age--;
        }
        if (age > 1900) {
            age -= 1900;
        }
        if (age >= 18 && age < 1900) {
            return true;
        }
        else
            return false;
    };
    FormularioUno.prototype.validacionesExtras = function (date) {
        return this.validarEdad(date);
    };
    return FormularioUno;
}(__WEBPACK_IMPORTED_MODULE_0__formulario__["a" /* Formulario */]));

//# sourceMappingURL=FormularioUno.js.map

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormularioDos; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__formulario__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_registrar_service__ = __webpack_require__(98);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var FormularioDos = (function (_super) {
    __extends(FormularioDos, _super);
    function FormularioDos(complete) {
        var _this = _super.call(this) || this;
        _this.complete = complete;
        _this.formBuilder = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */]();
        return _this;
    }
    FormularioDos.prototype.GetFormBuilder = function () {
        var _registrar = new __WEBPACK_IMPORTED_MODULE_2__services_registrar_service__["a" /* RegistrarService */]();
        _registrar.registrar(this);
        if (!this.complete) {
            //si queremos ingresar la direccion detallada
            this.form = this.formBuilder.group({
                pais: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
                ciudad: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern("^[a-zA-Z_áéíóúñ\s]*$")],
                direccion: ['', ''],
                cod: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern("[0-9]*")]
            });
        }
        else {
            // si no queremos ingresar la direccion detallada
            this.form = this.formBuilder.group({
                pais: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern("^[a-zA-Z_áéíóúñ\s]*$")]],
                ciudad: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern("^[a-zA-Z_áéíóúñ\s]*$")]],
                direccion: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
                cod: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern("[0-9]*")]]
            });
        }
        return this.form;
    };
    FormularioDos.prototype.presentAlert = function (form, alertCtrl) {
        var alert = alertCtrl.create({
            title: 'Datos',
            subTitle: 'Guardado Exitosamente!',
            buttons: ['Dismiss']
        });
        alert.present();
    };
    return FormularioDos;
}(__WEBPACK_IMPORTED_MODULE_0__formulario__["a" /* Formulario */]));

//# sourceMappingURL=FormularioDos.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the HeaderComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var HeaderComponent = (function () {
    function HeaderComponent() {
        console.log('Hello HeaderComponent Component');
        this.text = 'Hello World';
    }
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-header',template:/*ion-inline-start:"/home/adonisrq94/Escritorio/ejercicio-3/src/pages/header/header.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n        <ion-icon class="icono-more" ios="ios-more" md="md-more"></ion-icon>\n        <ion-icon class="icono" ios="ios-bookmark" md="md-bookmark"></ion-icon>\n        <ion-icon class="icono" ios="ios-share" md="md-share"></ion-icon>\n\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n'/*ion-inline-end:"/home/adonisrq94/Escritorio/ejercicio-3/src/pages/header/header.html"*/
    }),
    __metadata("design:paramtypes", [])
], HeaderComponent);

//# sourceMappingURL=header.js.map

/***/ }),

/***/ 98:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistrarService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RegistrarService = (function () {
    function RegistrarService() {
        this.apiGooglePais = [
            { id: 1, name: 'venezuela' },
            { id: 2, name: 'mexico' }
        ];
        this.apiGoogleCiudad = [
            { id: 1, name: 'caracas' },
            { id: 2, name: 'tachira' }
        ];
    }
    RegistrarService.prototype.registrar = function (data) {
        this.data = data;
    };
    RegistrarService.prototype.getApiGoogleaPais = function () {
        setTimeout(function () { }, 10000);
        return this.apiGooglePais;
    };
    RegistrarService.prototype.getApiGoogleCiudad = function () {
        setTimeout(function () { }, 10000);
        return this.apiGoogleCiudad;
    };
    return RegistrarService;
}());
RegistrarService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], RegistrarService);

//# sourceMappingURL=registrar.service.js.map

/***/ })

},[196]);
//# sourceMappingURL=main.js.map
import {FormularioUno} from '../classes/FormularioUno';
import {FormularioDos} from '../classes/FormularioDos';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController,AlertController } from 'ionic-angular';




 export class  FactoriaFormulario {

 constructor(public navC:NavController){}

  private formBuilder: FormBuilder;
  getFormularioUno():FormularioUno{
    return new FormularioUno(this.navC);
  }


  getFormularioDos(complete:boolean):FormularioDos{
    return new FormularioDos(complete);
  }





}

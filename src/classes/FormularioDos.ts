import {Formulario} from './formulario';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController,AlertController } from 'ionic-angular';
import {RegistrarService} from '../services/registrar.service';
export class FormularioDos extends Formulario {

  private form : FormGroup;
  private formBuilder: FormBuilder
  private complete:boolean;

constructor(complete:boolean){
   super();
   this.complete = complete;
   this.formBuilder = new FormBuilder();
}

 GetFormBuilder(): FormGroup{
   let _registrar:RegistrarService = new RegistrarService();
   _registrar.registrar(this);
   if (!this.complete) {
   //si queremos ingresar la direccion detallada
   this.form = this.formBuilder.group({
       pais: ['',Validators.required],
       ciudad: ['',Validators.pattern("^[a-zA-Z_áéíóúñ\s]*$")],
       direccion: ['',''],
       cod: ['',Validators.pattern("[0-9]*")]
    });
    }else{
      // si no queremos ingresar la direccion detallada
      this.form = this.formBuilder.group({
           pais: ['',[Validators.required,Validators.pattern("^[a-zA-Z_áéíóúñ\s]*$")]],
           ciudad: ['',[Validators.required,Validators.pattern("^[a-zA-Z_áéíóúñ\s]*$")]],
           direccion: ['',Validators.required],
           cod: ['',[Validators.required,Validators.pattern("[0-9]*")]]
        });
    }

   return this.form;
}




presentAlert(form:any,alertCtrl: AlertController) {

  let alert = alertCtrl.create({
    title: 'Datos',
    subTitle: 'Guardado Exitosamente!',
    buttons: ['Dismiss']
  });
  alert.present();
}






}

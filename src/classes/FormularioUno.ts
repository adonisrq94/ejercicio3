import {Formulario} from './formulario';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController,AlertController } from 'ionic-angular';
import {Formulario2Page} from '../pages/formularios-pages/formulario2/formulario2';
export class FormularioUno extends Formulario {



    private form : FormGroup;
    private formBuilder: FormBuilder

 constructor(public navC:NavController){
     super();
     this.formBuilder = new FormBuilder();
  }

  GetFormBuilder(): FormGroup{

   this.form = this.formBuilder.group({
        nombre: ['',[Validators.required,Validators.pattern("^[a-zA-Z_áéíóúñ\s]*$")]],
        apellido: ['',[Validators.required,Validators.pattern("^[a-zA-Z_áéíóúñ\s]*$")]],
        genero: ['',Validators.required],
        fecha: ['',Validators.required],
        identificacion: ['',[Validators.required,Validators.minLength(1),Validators.maxLength(5),Validators.pattern("[0-9]*")]],
        correo: ['',[Validators.required,Validators.pattern('^[a-z0-9A-Z_]+(\.[_a-z0-9A-Z]+)*@[a-z0-9-A-Z]+(\.[a-z0-9-A-Z]+)*(\.[a-zA-Z]{2,15})$')]],
     });

    return this.form;
   }


   presentConfirm(form:any, alertCtrl: AlertController) {
    let alert = alertCtrl.create({
    title: 'Confirmar',
    message: 'Deseas ingresar la direccion detallada?',
    buttons: [
      {
        text: 'si',
        role: 'cancel',
        handler: () => {
          form.opcion = true;
          this.navC.push(Formulario2Page,{form});

        }
      },
      {
        text: 'no',
        handler: () => {
          form.opcion = false;
          this.navC.push(Formulario2Page,{form});
        }
      }
    ]
  });
  alert.present();
  }


validarEdad(date:string):boolean{
  //validamos que sea mayor de edad
  date = date.replace('-','/') ;
  date = date.replace('-','/');
  let birthday = new Date(date);
  let year = birthday.getFullYear();
  let month = birthday.getMonth();
  let day = birthday.getDate();
  let dateToday = new Date();
  let yearToday = dateToday.getFullYear();
  let monthToday = dateToday.getMonth();
  let dayToday = dateToday.getDate();
  let age = (yearToday + 1900) - year;

   if ( monthToday < (age - 1)){
     age--;
   }
   if (((month - 1) == monthToday) && (dayToday < day)){
     age--;
   }
   if (age > 1900){
       age -= 1900;
   }

   if ( age >= 18 && age < 1900){
     return true;
   }else
   return false;
}

validacionesExtras(date:string){
  return this.validarEdad(date);
}


}

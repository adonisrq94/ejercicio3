import { Injectable } from '@angular/core';

@Injectable()
export class RegistrarService {
  private data:object
  public apiGooglePais:any[];
  public apiGoogleCiudad:any[];


  constructor() {
    this.apiGooglePais = [
       {id: 1, name:'venezuela'},
       {id: 2, name:'mexico'}
   ];
   this.apiGoogleCiudad = [
      {id: 1, name:'caracas'},
      {id: 2, name:'tachira'}
   ];}



  registrar(data:object){
       this.data = data;
  }


  getApiGoogleaPais():any[]{
    setTimeout(function(){},10000);
    return this.apiGooglePais;
  }

  getApiGoogleCiudad():any[]{
    setTimeout(function(){},10000);
    return this.apiGoogleCiudad;
  }


}

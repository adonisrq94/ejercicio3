import { Component } from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import {Formulario2Page} from '../formulario2/formulario2';
import {FactoriaFormulario} from '../../../factories/formulario';
import {Formulario} from '../../../classes/formulario';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-formulario1',
  templateUrl: 'formulario1.html'
})
export class Formulario1Page {

  private factoriaFormulario ;
  private formulario1 : FormGroup;
  private formulario : Formulario;

  constructor( private navC:NavController,private alertCtrl: AlertController) {

    this.factoriaFormulario = new FactoriaFormulario(this.navC);
    this.formulario =this.factoriaFormulario.getFormularioUno();
    this.formulario1 = this.formulario.GetFormBuilder();

  }

   irFormulario2(form=this.formulario1.value){

      if (this.formulario.validacionesExtras(form.fecha)){
          this.formulario.presentConfirm(form,this.alertCtrl);
      }else{
          this.formulario1.reset();
      }
   }





}

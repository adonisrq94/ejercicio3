import { Component } from '@angular/core';
import {RegistrarService} from '../../../services/registrar.service';
import {NavController, NavParams ,AlertController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {FactoriaFormulario} from '../../../factories/formulario';
import {Formulario} from '../../../classes/formulario';

@Component({
  selector: 'app-formulario2',
  templateUrl: 'formulario2.html'
})
export class Formulario2Page {

  private factoriaFormulario ;
  private formulario2 : FormGroup;
  private formulario : Formulario;
  public paises:any[];
  public ciudades:any[];

  constructor(_registrar:RegistrarService,
    public navC:NavController,
    private NavParams:NavParams,
    public alertCtrl: AlertController,
    private formBuilder: FormBuilder) {

    this.factoriaFormulario = new FactoriaFormulario(this.navC);
    this.formulario =this.factoriaFormulario.getFormularioDos(this.NavParams.data.form.opcion);
    this.formulario2 = this.formulario.GetFormBuilder();
    this.ciudades = _registrar.getApiGoogleCiudad();
    this.paises = _registrar.getApiGoogleaPais();






  }




     onChange(deviceValue) {
    console.log(deviceValue);
   }




     showAlert() {
       this.formulario.presentConfirm(this.formulario,this.alertCtrl)
     }



}
